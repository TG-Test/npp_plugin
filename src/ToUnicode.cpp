#include "PluginInterface.h"
#include "menuCmdID.h"
#include <map>
#include <string>

extern NppData nppData;

size_t toUnicode(char* source, char* destination)
{
	char* dest = destination;
	char* firstCh = nullptr;
	char pair = 0;
	size_t status = 0;
	bool marcoT = false;
	bool marcoL = false;
	bool skipLine = false;
	for (char* ch = source; *ch; ++ch)
	{
		if (*ch == '\r' || *ch == '\n')
		{
			pair = 0;
			status = 0;
			marcoT = false;
			marcoL = false;
			skipLine = false;
			firstCh = nullptr;
		}
		else if (!skipLine)
		{
			if (firstCh == nullptr)
			{
				firstCh = ch;
			}
			switch (status)
			{
			case 0:
				if (*ch == '#')
				{
					skipLine = true;
				}
				else if (*ch == '_')
				{
					char* chk = ch + 1;
					if (*chk && *chk == 'T')
					{
						++chk;
						if (*chk && *chk == '(')
						{
							status = 1;
							ch = chk;
							marcoT = true;
							continue;
						}
					}
				}
				else if (*ch == 'L')
				{
					if (*(ch + 1) == '"' || *(ch + 1) == '\'')
					{
						marcoL = true;
					}
				}
				else if (*ch == '"' || *ch == '\'')
				{
					pair = *ch;
					if (!marcoL)
					{
						*dest++ = 'L';
						marcoL = false;
					}
					status = 2;
				}
				break;
			case 1:
				if (*ch == ' ')
				{
					continue;
				}
				else if (*ch == '"' || *ch == '\'')
				{
					pair = *ch;
					*dest++ = 'L';
					status = 2;
				}
				break;
			case 2:
				if (*ch == L'\\')
				{
					status = 3;
				}
				else if (*ch == pair)
				{
					if (marcoT)
					{
						status = 4;
					}
					else
					{
						status = 0;
					}
				}
				break;
			case 3:
				status = 2;
				break;
			case 4:
				if (*ch == ' ')
				{
					continue;
				}
				else if (marcoT && *ch == ')')
				{
					marcoT = false;
					status = 0;
					continue;
				}
				status = 0;
				marcoT = false;
				break;
			}
		}
		*dest++ = *ch;

	}
	*dest = '\0';

	return (dest - destination) / sizeof(char);
}

bool ReplaceWFunc(std::string& Line)
{
	std::map<std::string, std::string> funcMap;
	funcMap["strchr("] = "wcschr(";
	funcMap["strcmp("] = "wcscmp(";
	funcMap["strcpy_s("] = "wcscpy_s(";
	funcMap["strcpy("] = "wcscpy(";
	funcMap["strlen("] = "wcslen(";
	funcMap["strrchr("] = "wcsrchr(";
	funcMap["strupr("] = "wcsupr(";
	funcMap["atoi("] = "_wtoi(";
	funcMap["atof("] = "_wtof(";
	funcMap["fopen_s("] = "_wfopen_s(";
	funcMap["fputs("] = "fputws(";
	funcMap["fprintf("] = "fwprintf(";
	funcMap["wsprintf("] = "wsprintfW(";
	funcMap["sprintf_s("] = "swprintf_s(";
	funcMap["sprintf("] = "swprintf(";
	funcMap["vfprintf("] = "vfwprintf(";
	funcMap["_unlink("] = "_wunlink(";
	funcMap["rename("] = "_wrename(";
	funcMap["system("] = "_wsystem(";

	bool ret = false;
	for (auto func : funcMap)
	{
		std::string::size_type pos = Line.find(func.first);
		if (pos == std::string::npos)
		{
			continue;
		}
		Line.replace(pos, func.first.length(), func.second);
		if (!ret)
		{
			ret = true;
		}
	}

	return ret;
}

void replaceFunc(std::string& line, char** dest)
{
	if (line.empty())
	{
		return;
	}
	ReplaceWFunc(line);
	for (std::string::size_type i = 0; i < line.length(); ++i)
	{
		*(*dest) = line.at(i);
		(*dest)++;
	}
	line.clear();
}

void toWFunc(char* source, char* destination)
{
	char* dest = destination;
	std::string line;
	for (char* ch = source; *ch; ++ch)
	{
		if (*ch == '\r' || *ch == '\n')
		{
			replaceFunc(line, &dest);
			*dest++ = *ch;
		}
		else
		{
			line += *ch;
		}
	}
	replaceFunc(line, &dest);
	*dest = '\0';
}

void ToUnicode()
{
	// Get the current scintilla handle
	int currentEdit;
	::SendMessage(nppData._nppHandle, NPPM_GETCURRENTSCINTILLA, 0, (LPARAM)&currentEdit);
	HWND curScint = (currentEdit == 0) ? nppData._scintillaMainHandle : nppData._scintillaSecondHandle;

	// get the text
	size_t len = ::SendMessage(curScint, SCI_GETLENGTH, 0, 0);
	if (len < 1)
	{
		return;
	}

	++len;
	char* source = new char[len];
	if (source == nullptr)
	{
		return;
	}
	::SendMessage(curScint, SCI_GETTEXT, len, (LPARAM)source);

	char* destination = new char[len * 2];
	if (destination == nullptr)
	{
		delete[] source;
		return;
	}

	size_t newLen = toUnicode(source, destination);

	++newLen;
	if (newLen > len)
	{
		delete[] source;
		source = new char[newLen];
	}
	memcpy(source, destination, sizeof(char)*(newLen));
	toWFunc(source, destination);

	// set the text of the file
	::SendMessage(curScint, SCI_SETTEXT, 0, (LPARAM)destination);

	// clean up
	delete[] source;
	delete[] destination;
}

void ToUnicodeAll()
{
	int files = (int)::SendMessage(nppData._nppHandle, NPPM_GETNBOPENFILES, 0, (LPARAM)PRIMARY_VIEW);
	if (files <= 0)
	{
		return;
	}

	wchar_t** fileNames = new wchar_t*[files];
	for (int i = 0; i < files; ++i)
	{
		fileNames[i] = new wchar_t[MAX_PATH];
	}

	int files2 = (int)::SendMessage(nppData._nppHandle, NPPM_GETOPENFILENAMESPRIMARY, (WPARAM)fileNames, (LPARAM)files);

	for (int i = 0; i < files2; ++i)
	{
		::SendMessage(nppData._nppHandle, NPPM_SWITCHTOFILE, (WPARAM)0, (LPARAM)fileNames[i]);
		ToUnicode();
	}

	for (int i = 0; i < files; ++i)
	{
		delete[] fileNames[i];
	}
	delete[] fileNames;

	return;
}

void ToUtf8BomAll()
{
	int files = (int)::SendMessage(nppData._nppHandle, NPPM_GETNBOPENFILES, 0, (LPARAM)PRIMARY_VIEW);
	if (files <= 0)
	{
		return;
	}

	wchar_t** fileNames = new wchar_t*[files];
	for (int i = 0; i < files; ++i)
	{
		fileNames[i] = new wchar_t[MAX_PATH];
	}

	int files2 = (int)::SendMessage(nppData._nppHandle, NPPM_GETOPENFILENAMESPRIMARY, (WPARAM)fileNames, (LPARAM)files);

	for (int i = 0; i < files2; ++i)
	{
		::SendMessage(nppData._nppHandle, NPPM_SWITCHTOFILE, (WPARAM)0, (LPARAM)fileNames[i]);
		::SendMessage(nppData._nppHandle, NPPM_MENUCOMMAND, 0, IDM_FORMAT_CONV2_UTF_8);
		::SendMessage(nppData._nppHandle, NPPM_MENUCOMMAND, 0, IDM_FILE_SAVE);
	}

	for (int i = 0; i < files; ++i)
	{
		delete[] fileNames[i];
	}
	delete[] fileNames;
}
